package net.openesb.tomcat;

import java.util.Properties;
import java.util.logging.Logger;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class TomcatFramework extends com.sun.jbi.framework.JBIFramework {

    /**
     * key for the install root property *
     */
    public static final String INSTALL_ROOT = "install.root";
    /**
     * key for the instance name property *
     */
    public static final String INSTANCE_NAME = "instance.name";
    /**
     * Configuration defaults.
     */
    private static final String DEFAULT_INSTALL_ROOT =
            System.getProperty("user.dir");
    private static final String DEFAULT_INSTANCE_NAME =
            "server";
    private TomcatPlatformContext mPlatformContext;
    private boolean mLoaded;
    private Properties mEnvironment;
    private Logger mLog =
            Logger.getLogger(this.getClass().getPackage().getName());

    /**
     * Creates a new instance of the JBI framework.
     *
     * @param environment the properties from servlet context
     */
    public TomcatFramework(Properties environment) {
        super();

        mEnvironment = environment;
        mPlatformContext = new TomcatPlatformContext(
                mEnvironment.getProperty(INSTANCE_NAME, DEFAULT_INSTANCE_NAME),
                mEnvironment.getProperty(INSTALL_ROOT, DEFAULT_INSTALL_ROOT));
    }

    /**
     * Load the OpenESB runtime with the specified environment.  When this method
     * retuns, all public interfaces and system services have completely
     * initialized.  If a connector port is specified in the environment
     * properties, a remote JMX connector server is created.
     *
     * @throws Exception failed to load OpenESB runtime
     */
    public void start()
            throws Exception {
        try {

            if (mLoaded) {
                throw new IllegalStateException("OpenESB already loaded!");
            }

            mEnvironment.setProperty("com.sun.jbi.home",
                    mPlatformContext.getInstallRoot());
            init(mPlatformContext, mEnvironment);
            startup(mPlatformContext.getNamingContext(), "");
            prepare();
            ready(true);

            // OpenESB framework has been loaded
            mLoaded = true;
        } catch (Exception ex) {
            mLog.severe(ex.getMessage());
        }
    }

    /**
     * Shutdown OpenESB runtime.  When this method retuns, all
     * public interfaces, system services, and JMX connector (if configured)
     * have been destroyed.
     *
     * @throws Exception failed to unload OpenESB runtime
     */
    public void stop()
            throws Exception {
        if (!mLoaded) {
            return;
        }

        shutdown();
        terminate();

        mLoaded = false;
    }
}

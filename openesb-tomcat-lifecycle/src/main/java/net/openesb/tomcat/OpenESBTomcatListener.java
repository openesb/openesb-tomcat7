package net.openesb.tomcat;

import org.apache.catalina.Lifecycle;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleListener;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class OpenESBTomcatListener implements LifecycleListener {

    private static Log log = LogFactory.getLog(OpenESBTomcatListener.class);

    protected TomcatFramework tomcatFramework;

    @Override
    public void lifecycleEvent(LifecycleEvent event)
    {
        if (Lifecycle.BEFORE_START_EVENT.equals(event.getType()))
        {
            if (log.isDebugEnabled())
            {
                log.debug("Before start event : OpenESB");
            }
            doStart();
            return;
        }

        if (Lifecycle.BEFORE_STOP_EVENT.equals(event.getType()))
        {
            if (log.isDebugEnabled())
            {
                log.debug("Before stop event : OpenESB");
            }
            doStop();
            return;
        }

        if (log.isDebugEnabled())
        {
            log.debug("OpenESB can't handle this event: " + event.getType());
        }
    }

    protected void doStart()
    {
        log.info("Starting OpenESB");
        try
        {
            tomcatFramework = new TomcatFramework(System.getProperties());
            tomcatFramework.start();
        }
        catch (Exception e)
        {
            log.error("Failed to start OpenESB", e);
        }
    }

    protected void doStop()
    {
        log.info("Stopping OpenESB");
        try
        {
            tomcatFramework.stop();
        }
        catch (Exception e)
        {
            // Ridiculous juli bugs - logger would have already been disposed
            // by a shutdown handler by now
            System.err.println("Failed to stop OpenESB: " + e);
        }
    }

}
